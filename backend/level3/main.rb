require 'date'
require 'json'

class Car
  attr_accessor :id, :price_per_day, :price_per_km

  def initialize id, price_per_day, price_per_km
    @id = id
    @price_per_day = price_per_day
    @price_per_km = price_per_km
  end
end

class Rental
  attr_accessor :id, :car_id, :start_date, :end_date, :distance

  def initialize id, car_id, start_date, end_date, distance
    @id = id
    @car_id = car_id
    @start_date = start_date
    @end_date = end_date
    @distance = distance
  end

  def price price_per_day, price_per_km
    diff_date = (Date.parse(@end_date) - Date.parse(@start_date)).to_i + 1
    amount_by_date = 0

    while diff_date > 0
      sale = 0
      if diff_date > 1 && diff_date <= 4
        sale = 0.1
      elsif diff_date > 4 && diff_date <= 10
        sale = 0.3
      elsif diff_date > 10
        sale = 0.5
      end
      amount_by_date += price_per_day * (1 - sale)
      diff_date -= 1
    end

    price = amount_by_date + price_per_km * @distance
    price.to_i
  end

  def commission price_per_day, price_per_km
    diff_date = (Date.parse(@end_date) - Date.parse(@start_date)).to_i + 1
    commission = price(price_per_day, price_per_km) * 0.3
    insurance_fee = commission / 2
    assistance_fee = 100 * diff_date
    drivy_fee = insurance_fee - assistance_fee
    { insurance_fee: insurance_fee.to_i, assistance_fee: assistance_fee.to_i, drivy_fee: drivy_fee.to_i }
  end
end

input = File.read 'data/input.json'
data = JSON.parse input

cars = data['cars'].map { |car| Car.new car['id'], car['price_per_day'], car['price_per_km'] }
rentals = data['rentals'].map { |rental| Rental.new rental['id'], rental['car_id'], rental['start_date'], rental['end_date'], rental['distance'] }

output = {}
output['rentals'] = []
rentals.each do |rental|
  car = cars.find { |car| car.id == rental.car_id }
  price = rental.price car.price_per_day, car.price_per_km
  obj = { 'id' => rental.id, 'price' => price }
  commission = rental.commission car.price_per_day, car.price_per_km
  obj['commission'] = { 'insurance_fee' => commission[:insurance_fee], 'assistance_fee' => commission[:assistance_fee], 'drivy_fee' => commission[:drivy_fee]}
  output['rentals'] << obj
end

expected_file = File.read 'data/expected_output.json'
expected_data = JSON.parse expected_file

if output == expected_data
  puts 'success'
else
  puts 'fail'
end

