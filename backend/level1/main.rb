require 'date'
require 'json'

class Car
  attr_accessor :id, :price_per_day, :price_per_km

  def initialize id, price_per_day, price_per_km
    @id = id
    @price_per_day = price_per_day
    @price_per_km = price_per_km
  end
end

class Rental
  attr_accessor :id, :car_id, :start_date, :end_date, :distance

  def initialize id, car_id, start_date, end_date, distance
    @id = id
    @car_id = car_id
    @start_date = start_date
    @end_date = end_date
    @distance = distance
  end

  def price price_per_day, price_per_km
    diff_date = (Date.parse(@end_date) - Date.parse(@start_date)).to_i + 1
    price_per_day * diff_date + price_per_km * @distance
  end
end

input = File.read 'data/input.json'
data = JSON.parse input

cars = data['cars'].map { |car| Car.new car['id'], car['price_per_day'], car['price_per_km'] }
rentals = data['rentals'].map { |rental| Rental.new rental['id'], rental['car_id'], rental['start_date'], rental['end_date'], rental['distance'] }

output = {}
output['rentals'] = []
rentals.each do |rental|
  car = cars.find { |car| car.id == rental.car_id }
  price = rental.price car.price_per_day, car.price_per_km
  output['rentals'] << { 'id' => rental.id, 'price' => price }
end

expected_file = File.read 'data/expected_output.json'
expected_data = JSON.parse expected_file

if output == expected_data
  puts 'success'
else
  puts 'fail'
end
